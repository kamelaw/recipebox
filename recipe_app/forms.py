"""
class Author(models.Model):
    name = models.CharField(max_length=150)
    bio = models.TextField()

    def __str__(self):
        return self.name


class Recipe(models.Model):
    title = models.CharField(max_length=40)
    # foreignkey needs to link to another instance of a model
    # maybe SET_NULL instead of CASCADE
    # SET_NULL would keep recipes even if user is deleted. I think
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    description = models.TextField()
    timerequired = models.CharField(max_length=20)
    instructions = models.TextField()

    def __str__(self):
        return f"{self.title} | {self.author}"
"""

from django import forms
from recipe_app.models import Author

class AddRecipeForm(forms.Form):
    title = forms.CharField(max_length=40)
    author = forms.ModelChoiceField(queryset=Author.objects.all())
    description = forms.CharField(widget=forms.Textarea)
    timerequired = forms.CharField(max_length=50)
    instructions = forms.CharField(widget=forms.Textarea)


class AddAuthorForm(forms.Form):
    name = forms.CharField(max_length=80)
    bio = forms.CharField(widget=forms.Textarea)
    username = forms.CharField(max_length=80)
    password = forms.CharField(widget=forms.PasswordInput)
    


class LoginForm(forms.Form):
    username = forms.CharField(max_length=80)
    password = forms.CharField(widget=forms.PasswordInput)
