from django.shortcuts import render, HttpResponseRedirect, reverse
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden

from recipe_app.models import Author, Recipe
from recipe_app.forms import AddRecipeForm, AddAuthorForm, LoginForm


# Create your views here.
# A view is a place where we put the "logic" of our application. It will 
# request information from the model you created before 
# and pass it to a template
# in demo Joe uses articles and my equivalent is allrecipes
def index(request):
    allrecipes = Recipe.objects.all()
    return render(request, "index.html", {
        'heading': "RECIPE BOX", "allrecipes": allrecipes
    })


"""
localhost:8000/recipe/4
"""


def recipe_detail(request, post_id):
    recipes = Recipe.objects.get(id=post_id)
    return render(request, "recipe_detail.html", {"recipes": recipes})


# function that takes in request & shows 1 thing
# in demo Joe uses article & my equivalent is recipes
def author_detail(request, author_id):
    author_obj = Author.objects.get(id=author_id)
    # why doesn't allrecipes work here
    recipes = Recipe.objects.filter(author=author_obj)

    return render(request, "author_detail.html", {
        "author": author_obj,
        "recipes": recipes
    })


@login_required
def add_recipe(request):
    context = {}
    if request.method == 'POST':
        form = AddRecipeForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            new_item = Recipe.objects.create(
                title=data['title'],
                author=data['author'],
                description=data['description'],
                timerequired=data['timerequired'],
                instructions=data['instructions']

            )
            # did have recipe_detail where homepage is and afterhomepage
            # had args=[new_item.id]
            return HttpResponseRedirect(reverse('homepage'))

    form = AddRecipeForm
    context.update({'form': form})
    return render(
        request,
        "generic_form.html",
        context
    )


@login_required
def add_author(request):
    # must be staff so need to verify
    if request.user.is_staff:
        if request.method == 'POST':
            form = AddAuthorForm(request.POST)
            if form.is_valid():
                data = form.cleaned_data
                new_user = User.objects.create_user(
                    username=data['username'],
                    password=data['password']
                )
                Author.objects.create(
                    name=data['username'],
                    bio=data['bio'],
                    user=new_user
                )
        
            return HttpResponseRedirect(reverse('homepage'))
    else:
        return HttpResponseForbidden(
            'You are not authorized to complete this action!')


    form = AddAuthorForm
    return render(request, 'generic_form.html', {'form': form})


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(
                request, username=data['username'], password=data['password']
            )
            if user:
                login(request, user)

                return HttpResponseRedirect(request.GET.get('next', reverse('homepage')))

    form = LoginForm()
    return render(request, 'generic_form.html', {'form': form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('homepage'))
