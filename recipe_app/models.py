from django.db import models
from django.contrib.auth.models import User

# Create your models here.
"""

User:
---
username
password 
email

|will interact with user on 1 to 1 basis
1 element on either side of the relationship. 1 user model can be tied to 
1 author model. intrinsically linking those 2 things


Author model:

Name (CharField)
Bio (TextField)
1 to many


Recipe Model:

Title (CharField)
Author (ForeignKey) foreignkey is 1 to many relationship
Description (TextField)
Time Required (Charfield) (for example, "One hour")
Instructions (TextField)


"""
# implementing author first because it's the easiest of the 2


class Author(models.Model):
    name = models.CharField(max_length=150)
    bio = models.TextField()
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Recipe(models.Model):
    title = models.CharField(max_length=80)
    # foreignkey needs to link to another instance of a model
    # maybe SET_NULL instead of CASCADE
    # SET_NULL would keep recipes even if user is deleted. I think
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    description = models.TextField()
    timerequired = models.CharField(max_length=50)
    instructions = models.TextField()

    def __str__(self):
        return f"{self.title} | {self.author}"
